using Moq;
using RewardsManager.CurrencyExchange.Interfaces;
using RewardsManager.Entities;
using RewardsManager.Repository.Interfaces;
using RewardsManager.RewardStrategy;
using RewardsManager.RewardStrategy.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace RewardsManager.Tests
{
    public class TransactionServiceTest
    {
        [Fact]
        public async Task MakeTransaction_ApplyAllRewards()
        {
            //arrange
            var transaction = new Transaction
            {
                Account = new Account { Currency = Currency.UAH, Balance = 10000m },
                ChangeBalanceDelta = new Money { Currency = Currency.UAH, Amount = 1000m },
                UserId = 1
            };

            IEnumerable<Money> rewards = null;
            var accountRepositoryMock = new Mock<IAccountRepository>();
            accountRepositoryMock.Setup(
                m => m.ApplyRewardAsync(It.IsAny<IEnumerable<Money>>(), It.IsAny<int>()))
                .Callback<IEnumerable<Money>, int>((appliedRewards, userId) => rewards = appliedRewards);
            
            var transactionRepositoryMock = new Mock<ITransactionRepository>();
            transactionRepositoryMock.Setup(
                m => m.GetSpendTransactionsAsync(It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<DateTime>()))
                .Returns(Task.FromResult(Enumerable.Repeat(new Transaction(), 10)));
            
            var currencyExchangeMock = new Mock<ICurrencyExchange>();
            currencyExchangeMock.Setup(
                m => m.ExchangeCurrencyAsync(It.IsAny<Money>(), It.IsAny<Currency>()))
                .Returns(Task.FromResult(
                    new Money { Currency = Currency.UAH, Amount = transaction.ChangeBalanceDelta.Amount }));
            
            var rewardStrategies = new List<IRewardStrategy>
            {
                new FiveUnitsMore(currencyExchangeMock.Object),
                new TenMoreOperationsPerDay(transactionRepositoryMock.Object)
            };

            //Act
            var service = new TransactionService(rewardStrategies, accountRepositoryMock.Object);
            await service.MakeTransactionAsync(transaction);

            //Assert
            Assert.NotNull(rewards);
            accountRepositoryMock.Verify(
                m => m.ApplyRewardAsync(It.IsAny<IEnumerable<Money>>(), It.IsAny<int>()), Times.Once());
            
            Assert.Collection(rewards, 
                item => Assert.Equal(10m, item.Amount), //FiveUnitsMore reward
                item => Assert.Equal(100m, item.Amount)); // TenMoreOperationsPerDay reward
        }


        [Fact]
        public async Task MakeTransaction_ApplyFiveUnitsMoreRewardOnly()
        {
            //arrange
            var transaction = new Transaction
            {
                Account = new Account { Currency = Currency.UAH, Balance = 10000m },
                ChangeBalanceDelta = new Money { Currency = Currency.UAH, Amount = 1000m },
                UserId = 1
            };

            IEnumerable<Money> rewards = null;
            var accountRepositoryMock = new Mock<IAccountRepository>();
            accountRepositoryMock.Setup(
                m => m.ApplyRewardAsync(It.IsAny<IEnumerable<Money>>(), It.IsAny<int>()))
                .Callback<IEnumerable<Money>, int>((appliedRewards, userId) => rewards = appliedRewards);

            var transactionRepositoryMock = new Mock<ITransactionRepository>();
            transactionRepositoryMock.Setup(
                m => m.GetSpendTransactionsAsync(It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<DateTime>()))
                .Returns(Task.FromResult(Enumerable.Repeat(new Transaction(), 1)));

            var currencyExchangeMock = new Mock<ICurrencyExchange>();
            currencyExchangeMock.Setup(
                m => m.ExchangeCurrencyAsync(It.IsAny<Money>(), It.IsAny<Currency>()))
                .Returns(Task.FromResult(
                    new Money { Currency = Currency.UAH, Amount = transaction.ChangeBalanceDelta.Amount }));

            var rewardStrategies = new List<IRewardStrategy>
            {
                new FiveUnitsMore(currencyExchangeMock.Object),
                new TenMoreOperationsPerDay(transactionRepositoryMock.Object)
            };

            //Act
            var service = new TransactionService(rewardStrategies, accountRepositoryMock.Object);
            await service.MakeTransactionAsync(transaction);

            //Assert
            Assert.NotNull(rewards);
            accountRepositoryMock.Verify(
                m => m.ApplyRewardAsync(It.IsAny<IEnumerable<Money>>(), It.IsAny<int>()), Times.Once());

            Assert.Collection(rewards,
                item => Assert.Equal(10m, item.Amount), //FiveUnitsMore reward
                item => Assert.Equal(0m, item.Amount)); // TenMoreOperationsPerDay reward
        }

        [Fact]
        public async Task MakeTransaction_ApplyTenMoreOperationsPerDayRewardOnly()
        {
            //arrange
            var transaction = new Transaction
            {
                Account = new Account { Currency = Currency.UAH, Balance = 10000m },
                ChangeBalanceDelta = new Money { Currency = Currency.UAH, Amount = 2m },
                UserId = 1
            };

            IEnumerable<Money> rewards = null;
            var accountRepositoryMock = new Mock<IAccountRepository>();
            accountRepositoryMock.Setup(
                m => m.ApplyRewardAsync(It.IsAny<IEnumerable<Money>>(), It.IsAny<int>()))
                .Callback<IEnumerable<Money>, int>((appliedRewards, userId) => rewards = appliedRewards);

            var transactionRepositoryMock = new Mock<ITransactionRepository>();
            transactionRepositoryMock.Setup(
                m => m.GetSpendTransactionsAsync(It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<DateTime>()))
                .Returns(Task.FromResult(Enumerable.Repeat(new Transaction(), 10)));

            var currencyExchangeMock = new Mock<ICurrencyExchange>();
            currencyExchangeMock.Setup(
                m => m.ExchangeCurrencyAsync(It.IsAny<Money>(), It.IsAny<Currency>()))
                .Returns(Task.FromResult(
                    new Money { Currency = Currency.UAH, Amount = transaction.ChangeBalanceDelta.Amount }));

            var rewardStrategies = new List<IRewardStrategy>
            {
                new FiveUnitsMore(currencyExchangeMock.Object),
                new TenMoreOperationsPerDay(transactionRepositoryMock.Object)
            };

            //Act
            var service = new TransactionService(rewardStrategies, accountRepositoryMock.Object);
            await service.MakeTransactionAsync(transaction);

            //Assert
            Assert.NotNull(rewards);
            accountRepositoryMock.Verify(
                m => m.ApplyRewardAsync(It.IsAny<IEnumerable<Money>>(), It.IsAny<int>()), Times.Once());

            Assert.Collection(rewards,
                item => Assert.Equal(0m, item.Amount), //FiveUnitsMore reward
                item => Assert.Equal(100m, item.Amount)); // TenMoreOperationsPerDay reward
        }
    }
}
