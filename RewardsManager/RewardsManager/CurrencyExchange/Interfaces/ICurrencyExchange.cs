﻿using RewardsManager.Entities;
using System.Threading.Tasks;

namespace RewardsManager.CurrencyExchange.Interfaces
{
    public interface ICurrencyExchange
    {
        Task<Money> ExchangeCurrencyAsync(Money money, Currency targetCurrency);
    }
}
