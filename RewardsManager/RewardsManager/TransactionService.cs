﻿using RewardsManager.Entities;
using RewardsManager.Repository.Interfaces;
using RewardsManager.RewardStrategy.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RewardsManager
{
    public class TransactionService : ITransactionService
    {
        private readonly IEnumerable<IRewardStrategy> rewardStrategies;
        private readonly IAccountRepository accountRepository;

        public TransactionService(IEnumerable<IRewardStrategy> rewardStrategies, IAccountRepository accountRepository)
        {
            this.rewardStrategies = rewardStrategies;
            this.accountRepository = accountRepository;
        }

        public async Task MakeTransactionAsync(Transaction transaction)
        {
            var tasks = rewardStrategies.Select(it => it.ApplyRewardAsync(transaction));
            var rewards = await Task.WhenAll(tasks.ToArray());

            await accountRepository.ApplyRewardAsync(rewards, transaction.UserId);
            
            //Further business logic
        }
    }
}
