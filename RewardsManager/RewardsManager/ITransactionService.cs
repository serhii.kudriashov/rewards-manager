﻿using RewardsManager.Entities;
using System.Threading.Tasks;

namespace RewardsManager
{
    public interface ITransactionService
    {
        Task MakeTransactionAsync(Transaction transaction);
    }
}
