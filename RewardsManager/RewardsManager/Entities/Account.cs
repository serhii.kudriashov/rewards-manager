﻿namespace RewardsManager.Entities
{
    public class Account
    {
        public decimal Balance { get; set; }
        public Currency Currency { get; set; }
    }
}
