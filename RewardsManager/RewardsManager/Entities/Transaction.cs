﻿namespace RewardsManager.Entities
{
    public class Transaction
    {
        public int UserId { get; set; }
        public Account Account { get; set; }
        public Money ChangeBalanceDelta { get; set; }
    }
}
