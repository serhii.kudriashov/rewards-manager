﻿using RewardsManager.Entities;
using System.Threading.Tasks;

namespace RewardsManager.RewardStrategy.Interfaces
{
    public interface IRewardStrategy
    {
        Task<Money> ApplyRewardAsync(Transaction transaction);
    }
}
