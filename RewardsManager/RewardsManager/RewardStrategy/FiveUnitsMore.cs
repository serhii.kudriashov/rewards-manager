﻿using RewardsManager.CurrencyExchange.Interfaces;
using RewardsManager.Entities;
using RewardsManager.RewardStrategy.Interfaces;
using System.Threading.Tasks;

namespace RewardsManager.RewardStrategy
{
    /// <summary>
    /// The user spends more than 5 currency units (5 USD, 5 EUR, etc.) from the account (1% bonus)
    /// </summary>
    public class FiveUnitsMore : IRewardStrategy
    {
        //TODO: Inject it with options pattern
        private readonly decimal bonusPercent = 1;

        private readonly ICurrencyExchange currencyExchange;

        public FiveUnitsMore(ICurrencyExchange currencyExchange)
        {
            this.currencyExchange = currencyExchange;
        }

        public async Task<Money> ApplyRewardAsync(Transaction transaction)
        {
            //convert transaction' currency to account' currency
            var changeBalance =
                await currencyExchange.ExchangeCurrencyAsync(transaction.ChangeBalanceDelta, transaction.Account.Currency);

            if (changeBalance.Amount > 5m)
            {
                return new Money
                {
                    Currency = changeBalance.Currency,
                    Amount = changeBalance.Amount / 100.0m * bonusPercent // calculate {bonusPercent} from transaction amount
                };
            }
            return new Money { Amount = 0m, Currency = changeBalance.Currency };
        }
    }
}
