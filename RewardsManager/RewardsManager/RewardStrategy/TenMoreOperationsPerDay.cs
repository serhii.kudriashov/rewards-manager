﻿using RewardsManager.Entities;
using RewardsManager.Repository.Interfaces;
using RewardsManager.RewardStrategy.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RewardsManager.RewardStrategy
{
    /// <summary>
    /// Makes 10+ spend operations per day (additional 1% of total amount)
    /// </summary>
    public class TenMoreOperationsPerDay : IRewardStrategy
    {
        //TODO: Inject it with options pattern
        private readonly decimal bonusPercent = 1;
        private readonly decimal transactionsPerDayCount = 10;

        private readonly ITransactionRepository transactionRepository;

        public TenMoreOperationsPerDay(ITransactionRepository transactionRepository)
        {
            this.transactionRepository = transactionRepository;
        }

        public async Task<Money> ApplyRewardAsync(Transaction transaction)
        {
            var today = DateTime.Today;
            var transactions =
                await transactionRepository.GetSpendTransactionsAsync(transaction.UserId, today, today.AddDays(1));

            var transactionsCount = transactions.Count();
            if (transactionsCount == transactionsPerDayCount)
            {
                var initialBalance = transaction.Account.Balance;
                return new Money
                {
                    Currency = transaction.Account.Currency,
                    Amount = initialBalance / 100.0m * bonusPercent // calculate {bonusPercent} from total account balance
                };
            }
            return new Money { Amount = 0m, Currency = transaction.Account.Currency };
        }
    }
}
