﻿using RewardsManager.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RewardsManager.Repository.Interfaces
{
    public interface IAccountRepository
    {
        /// <summary>
        /// Change bonus account of the user
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="balance"></param>
        /// <returns></returns>
        Task ApplyRewardAsync(IEnumerable<Money> rewards, int userId);
    }
}
