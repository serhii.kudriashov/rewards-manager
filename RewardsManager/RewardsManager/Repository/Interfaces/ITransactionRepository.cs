﻿using RewardsManager.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RewardsManager.Repository.Interfaces
{
    public interface ITransactionRepository
    {
        Task<IEnumerable<Transaction>> GetSpendTransactionsAsync(int userId, DateTime from, DateTime to);
    }
}
